from flask import Flask, request, jsonify
from csv import DictReader, DictWriter
import os
from http import HTTPStatus
from services import get_last_id, get_users, get_user_by_id, users


app = Flask(__name__)


FILENAME = 'users.csv'
FIELDNAMES = ['id', 'name', 'age', 'email', 'password']


@app.route('/signup', methods=['POST'])
def create_product():
    if not os.path.exists(FILENAME) or os.stat(FILENAME).st_size == 0:
        with open(FILENAME, 'w') as f:
            writer = DictWriter(f, fieldnames=FIELDNAMES)
            writer.writeheader()

    user = request.get_json()
    user['id'] = get_last_id(FILENAME)

    with open(FILENAME, 'r') as f:
        reader = DictReader(f)
        for u in reader:
            if user['email'] == u['email']:
                return {}, HTTPStatus.UNPROCESSABLE_ENTITY 
            
        with open(FILENAME, 'a') as f:
            writer = DictWriter(f, fieldnames=FIELDNAMES)
            writer.writerow(user) 
                
    return {
        "id": user['id'], 
        "name": user['name'],
        "age": user['age'],
        "email": user['email']
        }    


@app.route('/login', methods=['POST'])
def login():
    user_info = request.get_json()

    with open(FILENAME, 'r') as f:
        reader = DictReader(f)
        for user in reader:
            if user_info['email'] == user['email'] and user_info['password'] == user['password']:
                return {
                    "id": user['id'], 
                    "name": user['name'],
                    "age": user['age'],
                    "email": user['email']
                    }

    return {}, HTTPStatus.NOT_FOUND


@app.route('/profile/<int:user_id>', methods=['PATCH'])
def update_user(user_id):
    users = get_users(FILENAME)
    user_info = request.get_json()

    for user in users:
        if user['id'] == str(user_id):
            user.update(user_info)
            print(users)

            with open(FILENAME, 'w') as f:
                writer = DictWriter(f, fieldnames=FIELDNAMES)
                writer.writeheader()
                writer.writerows(users)
                
            return {
                "id": user['id'],
                "name": user['name'],
                "age": user['age'],
                "email": user['email']
                }

    return {}, HTTPStatus.NOT_FOUND


@app.route('/profile/<int:user_id>', methods=['DELETE'])
def delete_user(user_id):
    user = get_user_by_id(FILENAME, user_id)
    users = get_users(FILENAME)

    if user:
        users.remove(user)

        with open(FILENAME, 'w') as f:
            writer = DictWriter(f, fieldnames=FIELDNAMES)
            writer.writeheader()
            writer.writerows(users)

        return {}, HTTPStatus.NO_CONTENT

    return {}, HTTPStatus.NOT_FOUND


@app.route('/users')
def all_users():
    return jsonify(get_users(FILENAME))
