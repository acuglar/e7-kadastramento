from csv import DictReader

def get_users(FILENAME):

    with open(FILENAME, 'r') as f:
        reader = DictReader(f)
        data = list(reader)
    
    return data