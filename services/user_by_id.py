from csv import DictReader

def get_user_by_id(FILENAME, user_id):

    with open(FILENAME, 'r') as f:
        reader = DictReader(f)

        for user in reader:
            if int(user['id']) == user_id:
        
                return user

    return None