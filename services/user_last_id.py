from csv import DictReader

def get_last_id(FILENAME):

    with open(FILENAME, 'r') as f:
        reader = DictReader(f)
        data = list(reader)
    
    last_id = int(data[-1]['id']) + 1 if data else 1

    return last_id